# Pinterest Share Link

Couple helper classes that generate Pinterest share URL's.

## Usage

Use the product helper if you want to share the image of a product.

	<a href="<?php echo Mage::helper('pinterestsharelink/product')->init($_product) ?>">Share!</a>

Check this for possible options: https://developers.pinterest.com/pin_it/

Use the generic helper if you want to share any image.

    $options = array(
        'description'   => 'Hello World!',
        'media'         => 'http://example.com/hello.jpg'
    );

    echo Mage::helper('pinterestsharelink')->generateUrl(
    	//If specified, this will be the url that's shared in addition to the image.
    	//If not specified, it will take the current URL
        null,
        $options
    );