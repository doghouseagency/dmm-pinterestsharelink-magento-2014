<?php

/**
 * @todo  add rich pinning: https://developers.pinterest.com/rich_pins/
 */
class Doghouse_PinterestShareLink_Helper_Product
    extends Mage_Core_Helper_Abstract
{

	public function init(Mage_Catalog_Model_Product $product)
    {
		$this->_product = $product;
		return $this;
	}

	public function __toString()
    {

    	$p = $this->_product;

		$parms = array(
		    'description'   => Mage::helper('catalog/output')->productAttribute($p, $p->getName(), 'name'),
		    'media'       	=> (string)Mage::helper('catalog/image')->init($p, 'image')->resize(1000, 1000)
		);

		$shareurl = $p->getProductUrl();

		return Mage::helper('pinterestsharelink')->generateUrl($shareurl, $parms);

    }

}
