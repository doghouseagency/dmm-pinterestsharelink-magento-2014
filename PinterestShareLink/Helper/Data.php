<?php

class Doghouse_PinterestShareLink_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getDefaultShareUrl()
    {
        return Mage::helper('core/url')->getCurrentUrl();
    }

    /**
     * If shareurl, is empty, it uses the current URL
     *
     * For possible parameters, see https://developers.pinterest.com/pin_it/
     *
     *
     *
     * @param  [String] Share url
     * @param  [Array] $data options array
     * @return [String]       url
     */
    public function generateUrl($shareurl = null, $data = array())
    {

        /*
            Use it like this:

            $options = array(
                'description'   => $this->getText(),
                'media'         => $this->getImageUrl()
            );

            return Mage::helper('pinterestsharelink')->generateUrl(
                $this->parseUrl(),
                $options
            );

         */

        if(!$shareurl) {
            $shareurl = $this->getDefaultShareUrl();
        }

        $url = sprintf("http://www.pinterest.com/pin/create/button/?url=%s", urlencode($shareurl));

        if(is_array($data)) {

            foreach($data as $key => $value) {
                $url .= sprintf("&amp;%s=%s",
                    urlencode($key),
                    urlencode($value)
                );
            }

        }

        return $url;
    }

}
